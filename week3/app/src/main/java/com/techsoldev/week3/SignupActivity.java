package com.techsoldev.week3;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignupActivity extends AppCompatActivity {

    private EditText firstNameText, lastNameText ,emailText, passwordText,confirmText, userPhoneText, userRollno,addressText ;
    private CardView signupBtn;
    private TextView loginScreenLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        signupBtn = (CardView)findViewById(R.id.signup_btn);
        firstNameText = (EditText) findViewById(R.id.firstname);
        lastNameText= (EditText) findViewById(R.id.lastname);
        emailText= (EditText) findViewById(R.id.user_email);
        passwordText= (EditText) findViewById(R.id.user_password);
        confirmText = (EditText) findViewById(R.id.user_confirmpassword);
        userPhoneText = (EditText) findViewById(R.id.user_phone);
        userRollno= (EditText) findViewById(R.id.user_rollno);
        addressText= (EditText) findViewById(R.id.user_address);
        loginScreenLink = (TextView) findViewById(R.id.sigup_screen_link);








        userRollno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // if user is typing string one character at a time
                if (count == 1) {
                    // auto insert dashes while user typing their ssn
                    if ( start == 2) {
                        userRollno.setText(userRollno.getText() + "-");
                        userRollno.setSelection(userRollno.getText().length());
                    }
                }
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        userPhoneText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // if user is typing string one character at a time
                if (count == 1) {
                    // auto insert dashes while user typing their ssn
                    if ( start == 4) {
                        userPhoneText.setText(userPhoneText.getText() + "-");
                        userPhoneText.setSelection(userPhoneText.getText().length());
                    }
                }
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        loginScreenLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this,LoginActivity.class);
                startActivity(intent);

            }
        });

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkInputs();

            }
        });
    }


    void checkInputs() {
        String email_text = emailText.getText().toString();
        String confirmpass = confirmText.getText().toString();

        if (TextUtils.isEmpty(firstNameText.getText())) {
            firstNameText.setError("First name is required");
        }
        else if (TextUtils.isEmpty(lastNameText.getText())) {
            lastNameText.setError("last name is required");
        }
        else if (TextUtils.isEmpty(emailText.getText())) {
            emailText.setError("email is required");
        }
        else if (! Patterns.EMAIL_ADDRESS.matcher(email_text).matches())
        {
            emailText.setError("invalid email");
            emailText.requestFocus();
        }
        else if (TextUtils.isEmpty(firstNameText.getText())) {
            firstNameText.setError("First name is required");
        }
        else if (TextUtils.isEmpty(passwordText.getText())) {
            passwordText.setError("password is required");
        }
        else if(passwordText.getText().toString().length()<8 &&!isValidPassword(passwordText.getText().toString())){
            passwordText.setError("invalid password");
        }
        else if (TextUtils.isEmpty(confirmText.getText())) {
            confirmText.setError("password is required");
        }
        else if (! passwordText.getText().toString().trim().equals(confirmpass.trim()))
        {
            confirmText.setError("password not match");
        }
        else if (TextUtils.isEmpty(userRollno.getText())) {
            userRollno.setError("rollno is required");
        }
        else if (TextUtils.isEmpty(userPhoneText.getText())) {
            userPhoneText.setError("phone no is required");
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Account created");
            builder.setMessage("Now login !!");

            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    // Do nothing but close the dialog

                    dialog.dismiss();
                    Intent intent = new Intent(SignupActivity.this,LoginActivity.class);
                    finish();
                    startActivity(intent);

                }
            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();



        }

    }


        public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{8}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    }
